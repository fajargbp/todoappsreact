/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import TodoApp from './src/screen/TodoApp';
import store from './src/store'
import {Provider} from 'react-redux'

export default class App extends Component{
  render() {
    return (
      <Provider store={store}>
        <TodoApp/>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
