import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux';

class AddTodo extends Component {
    state={
        text:''
    }

    addTodo = (text) => {
        //redux store 
        this.props.dispatch({ type: 'ADD_TODO', text })
        this.setState({ text: '' })
    }

    render(){
        return(
            <View style={{flexDirection:'row', marginHorizontal:20}}>
                <TextInput 
                    onChangeText={(text)=>this.setState({text})}
                    placeholder="Masukkan To Do Anda"
                    style={{borderWidth:1, borderColor:'#f2f2e1', backgroundColor:'#eaeaea', flex:1, padding:10}}/>
                <TouchableOpacity onPress={() => this.addTodo(this.state.text)}>
                    <View style={{height:50, backgroundColor:'#eaeaea', alignItems:'center', justifyContent:'center'}}>
                        <Icon name='plus' size={30} style={{padding:10, color:'#de9595'}}/>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
};

export default connect()(AddTodo)