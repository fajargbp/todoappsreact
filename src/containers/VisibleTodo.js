import { connect } from 'react-redux';
import TodoList from '../component/TodoList'

const mapStateToProps = state => ({
    todos: state.todos
})

const mapDispatchToProps = dispatch => ({
    toogleTodo: id => dispatch({
        type: 'TOGGLE_TODO', id
    })
})

export default connect(mapStateToProps,mapDispatchToProps)(TodoList)