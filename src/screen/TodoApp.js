import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import AddTodo from '../containers/AddTodo'
import VisibleTodo from '../containers/VisibleTodo'

export default class TodoApp extends Component {
    // state={
    //     todos:[],
    //     visibilityFilter:'SHOW_ALL_TODOS'
    // }

    render(){
        return(
            <View style={styles.container}>
                <AddTodo/>
                <View>
                    <VisibleTodo/>
                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex:1,
        paddingTop: 30,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#fff'
    },
    textStyle: {
        color: '#000',
        fontWeight: 'bold',
        fontStyle: 'italic'
    }
});